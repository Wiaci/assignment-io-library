%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDIN 0
%define STDOUT 1

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor r9, r9
    .loop:
        cmp byte [rdi+r9], 0
        je .finish
        inc r9
        jmp .loop
    .finish:
        mov rax, r9
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, STDOUT
    mov rdx, rax
    mov rax, SYS_WRITE
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, "\n"
; Далее идет выполнение print_char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push 0
    mov r15, 10
    mov rax, rdi
    .loop:
        xor rdx, rdx
        div r15
        add rdx, '0'
        push rdx
        cmp rax, 0
        jne .loop
    .write:
        pop rdi
        cmp rdi, 0
        je .finish
        call print_char
        jmp .write
    .finish:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .write
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .write:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r9, r9
    xor r11, r11
    .loop:
        mov r9b, byte [rdi+rax]
        mov r11b, byte [rsi+rax]
        cmp r9, r11
        jne .not_equal
        cmp r9, 0
        je .equal
        inc rax
        jmp .loop
    .not_equal:
        mov rax, 0
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    mov rdi, STDIN
    mov rdx, 1
    mov rax, SYS_READ
    push 0
    mov rsi, rsp
    syscall
    pop rax
    pop rdi
    ret


is_tab_space_newline:
    cmp rdi, 0x20
    je .yes
    cmp rdi, 0x9
    je .yes
    cmp rdi, 0xA
    je .yes
    mov rax, 0
    ret
    .yes:
        mov rax, 1
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - buffer start pointer
; rsi - buffer size
read_word:
    mov r11, 1 
    mov r9, 0 
    .loop:
        push r11
        call read_char
        pop r11
        cmp rax, 0
        je .finish
        cmp r9, 0
        je .start
        cmp rax, " "
        je .finish
        cmp rax, "\t"
        je .finish
        cmp rax, "\n"
        je .finish
        jmp .write
    .start:
        push rdi
        mov rdi, rax
        call is_tab_space_newline
        cmp rax, 0
        mov rax, rdi
        pop rdi
        jne .loop
        mov r9, 1
    .write:
        inc r11
        cmp r11, rsi
        jg .failure
        mov byte [rdi+r11-2], al
        jmp .loop
    .failure:
        xor rax, rax
        ret
    .finish:
        dec r11
        mov byte [rdi+r11], 0
        mov rdx, r11
        mov rax, rdi
        ret

is_digit:
    mov r9, '0'
    mov r11, '9'
    cmp r12, r9
    jl .no
    cmp r12, r11
    jg .no
    mov rax, 1
    ret
    .no:
        mov rax, 0
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r12
    xor r9, r9
    xor rax, rax
    mov r11, 10
    .loop:
        xor r12, r12
        mov r12b, byte [rdi+r9]
        push rax
        push r9
        push r11
        call is_digit
        pop r11
        pop r9
        cmp rax, 0
        pop rax
        je .finish
        sub r12, '0'
        mul r11
        add rax, r12
        inc r9
        jmp .loop
    .finish:
        mov rdx, r9
        pop r12
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    xor r11, r11
    xor r12, r12
    mov r12b, byte [rdi]
    cmp r12, '-'
    jne .parse
    inc r11
    inc rdi
    .parse:
        push r11
        call parse_uint
        pop r11
        cmp r11, 0
        je .finish
        neg rax
        inc rdx
    .finish:
        pop r12
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - string pointer
; rsi - buffer start pointer
; rdx - buffer length
string_copy:
    push r11
    xor r11, r11
    push r9
	call string_length
    pop r9
    cmp rax, rdx
    jge .finish
    .loop:
        mov al, byte [rdi+r11]
        mov byte [rsi+r11], al
        cmp rax, 0
        je .finish
        inc r11
        jmp .loop
    .finish:
        mov rax, r11
        pop r11
        ret
